<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "chess".
 *
 * @property int $id
 * @property string $key
 * @property string $DataSdachi
 * @property string $DateActual
 * @property int $Etash
 * @property string $IDApartment
 * @property string $IDHouse
 * @property string $IDShka
 * @property string $NameShaka
 * @property string $Number
 * @property string $Podezd
 * @property int|null $Price2
 * @property int|null $Price3
 * @property int|null $PriceBase
 * @property string $SposobOtobrasheniya
 * @property string $Status
 * @property string|null $Top3kv
 * @property float $m2obshaya
 */
class Chess extends \yii\db\ActiveRecord
{
    public $IDApartment;

    public $IDHouse;

    public $IDShka;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chess';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'key',
                    'DataSdachi',
                    'DateActual',
                    'Etash',
                    'ID Apartment',
                    'ID House',
                    'ID Shka',
                    'IDApartment',
                    'IDHouse',
                    'IDShka',
                    'NameShaka',
                    'Number',
                    'Podezd',
                    'SposobOtobrasheniya',
                    'Status',
                    'm2obshaya',
                ],
                'required',
            ],
            [
                [
                    'key',
                    'ID Apartment',
                    'ID House',
                    'ID Shka',
                    'NameShaka',
                    'Number',
                    'Podezd',
                    'SposobOtobrasheniya',
                    'Status',
                    'Top3kv',
                ],
                'string',
            ],
            [['DataSdachi', 'DateActual'], 'safe'],
            [['Etash', 'Price2', 'Price3', 'PriceBase'], 'integer'],
            [['m2obshaya'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'DataSdachi' => Yii::t('app', 'Data Sdachi'),
            'DateActual' => Yii::t('app', 'Date Actual'),
            'Etash' => Yii::t('app', 'Etash'),
            'ID Apartment' => Yii::t('app', 'Id Apartment'),
            'IDApartment' => Yii::t('app', 'Id Apartment'),
            'ID House' => Yii::t('app', 'Id House'),
            'IDHouse' => Yii::t('app', 'Id House'),
            'ID Shka' => Yii::t('app', 'Id Shka'),
            'IDShka' => Yii::t('app', 'Id Shka'),
            'NameShaka' => Yii::t('app', 'Name Shaka'),
            'Number' => Yii::t('app', 'Number'),
            'Podezd' => Yii::t('app', 'Podezd'),
            'Price2' => Yii::t('app', 'Price2'),
            'Price3' => Yii::t('app', 'Price3'),
            'PriceBase' => Yii::t('app', 'Price Base'),
            'SposobOtobrasheniya' => Yii::t('app', 'Sposob Otobrasheniya'),
            'Status' => Yii::t('app', 'Status'),
            'Top3kv' => Yii::t('app', 'Top3kv'),
            'm2obshaya' => Yii::t('app', 'M2obshaya'),
        ];
    }
}
