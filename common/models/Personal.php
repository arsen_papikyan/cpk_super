<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "personal".
 *
 * @property int $id
 * @property string|null $key
 * @property string|null $Email
 * @property string|null $Family
 * @property string|null $ID Segment
 * @property string|null $Middle
 * @property string|null $Name
 * @property string|null $Phone
 * @property string|null $Position
 * @property string|null $password
 * @property string|null $image
 */
class Personal extends \yii\db\ActiveRecord
{
    public $IDSegment;
    public $img_name;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'Email', 'Family', 'ID Segment','IDSegment', 'Middle', 'image','img_name', 'Name', 'Phone', 'Position'], 'string'],
            [['password'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'img_name' => Yii::t('app', 'Img Name'),
            'Email' => Yii::t('app', 'Email'),
            'Family' => Yii::t('app', 'Family'),
            'ID Segment' => Yii::t('app', 'Id Segment'),
            'IDSegment' => Yii::t('app', 'Id Segment'),
            'Middle' => Yii::t('app', 'Middle'),
            'Name' => Yii::t('app', 'Name'),
            'Phone' => Yii::t('app', 'Phone'),
            'Position' => Yii::t('app', 'Position'),
            'password' => Yii::t('app', 'Password'),
            'image' => Yii::t('app', 'Image'),
        ];
    }
}
