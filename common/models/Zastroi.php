<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zastroi".
 *
 * @property int $id
 * @property string|null $key
 * @property string|null $AdressZ
 * @property string|null $DateActual
 * @property string|null $DocZ
 * @property string|null $NameZ
 * @property string|null $NameZ2
 * @property string|null $OfficeZ
 * @property string|null $OsnovanieZ
 * @property string|null $SdannieObgectZ
 * @property string|null $SiteZ
 * @property string|null $WatchJobZ
 * @property string|null $YpravZ
 */
class Zastroi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zastroi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'AdressZ', 'DocZ', 'NameZ', 'NameZ2', 'OfficeZ', 'OsnovanieZ', 'SdannieObgectZ', 'SiteZ', 'WatchJobZ', 'YpravZ'], 'string'],
            [['DateActual'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'AdressZ' => Yii::t('app', 'Adress Z'),
            'DateActual' => Yii::t('app', 'Date Actual'),
            'DocZ' => Yii::t('app', 'Doc Z'),
            'NameZ' => Yii::t('app', 'Name Z'),
            'NameZ2' => Yii::t('app', 'Name Z2'),
            'OfficeZ' => Yii::t('app', 'Office Z'),
            'OsnovanieZ' => Yii::t('app', 'Osnovanie Z'),
            'SdannieObgectZ' => Yii::t('app', 'Sdannie Obgect Z'),
            'SiteZ' => Yii::t('app', 'Site Z'),
            'WatchJobZ' => Yii::t('app', 'Watch Job Z'),
            'YpravZ' => Yii::t('app', 'Yprav Z'),
        ];
    }
}
