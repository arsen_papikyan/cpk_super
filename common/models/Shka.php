<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "shka".
 *
 * @property int $id
 * @property string|null $key
 * @property string|null $AdressShka
 * @property string|null $Banks
 * @property string|null $City
 * @property string|null $DateA
 * @property string|null $DateExpireid
 * @property string|null $DetskiePloshadki
 * @property string|null $DetskiySad
 * @property string|null $DrugieObekt1_3
 * @property string|null $DrugieObektSHD
 * @property string|null $Drugoe
 * @property string|null $Etash
 * @property string|null $Giper1_3
 * @property string|null $GiperSHD
 * @property string|null $GostParking
 * @property string|null $IDSegment
 * @property string|null $IdZastroi
 * @property string|null $KolLiter
 * @property string|null $Magazin1_3
 * @property string|null $MagazinSHD
 * @property string|null $Magazine
 * @property string|null $NameShka
 * @property string|null $NameShka2
 * @property string|null $NumberHouse
 * @property string|null $OfficeYpravCompany
 * @property string|null $Ohrana
 * @property string|null $OpisanieShka
 * @property string|null $Oplata
 * @property string|null $Ostanovka1_3
 * @property string|null $OstanovkaSHD
 * @property string|null $Park1_3
 * @property string|null $ParkSHD
 * @property string|null $Prem1
 * @property string|null $Prem2
 * @property string|null $Prem3
 * @property string|null $Prem4
 * @property string|null $Prem5
 * @property string|null $Prem6
 * @property int|null $PriceDo
 * @property int|null $PriceOt
 * @property string|null $Raion
 * @property string|null $Rassrochka
 * @property string|null $Region
 * @property string|null $Rinok1_3
 * @property string|null $RinokSHD
 * @property string|null $Sadik1_3
 * @property string|null $SadikSHD
 * @property string|null $Sells
 * @property string|null $Shkola
 * @property string|null $Shkola1_3
 * @property string|null $ShkolaSHD
 * @property string|null $Sport1_3
 * @property string|null $SportPloshadka
 * @property string|null $SportSHD
 * @property string|null $SposobOtotbrosheniya
 * @property string|null $Stadion
 * @property string|null $Top3Shka
 * @property string|null $VideoNabl
 * @property string|null $ZonaOtdih
 * @property int|null $m2Do
 * @property int|null $m2Ot
 * @property string|null $PromoName
 * @property string|null $IDApartment
 * @property string|null $maps
 * @property string|null $IM1
 * @property string|null $IM2
 * @property string|null $IM3
 * @property string|null $IM4
 * @property string|null $IM5
 * @property string|null $IM6
 * @property string|null $IM7
 * @property string|null $IM8
 * @property string|null $IM9
 * @property string|null $IM10
 * @property string|null $Action1
 * @property string|null $Action2
 * @property string|null $Action3
 * @property string|null $Action4
 * @property string|null $Action5
 */
class Shka extends \yii\db\ActiveRecord
{
    public $DrugieObekt1_3;

    public $Giper1_3;

    public $IDSegment;

    public $Magazin1_3;

    public $Ostanovka1_3;

    public $Park1_3;

    public $Rinok1_3;

    public $Sadik1_3;

    public $Shkola1_3;

    public $Sport1_3;

    public $IDApartment;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shka';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'key',
                    'AdressShka',
                    'Banks',
                    'City',
                    'DetskiePloshadki',
                    'DetskiySad',
                    'DrugieObekt1-3',
                    'DrugieObektSHD',
                    'Drugoe',
                    'Etash',
                    'Giper1-3',
                    'GiperSHD',
                    'GostParking',
                    'ID Segment',
                    'IdZastroi',
                    'KolLiter',
                    'Magazin1-3',
                    'MagazinSHD',
                    'Magazine',
                    'NameShka',
                    'NameShka2',
                    'NumberHouse',
                    'OfficeYpravCompany',
                    'Ohrana',
                    'OpisanieShka',
                    'Oplata',
                    'Ostanovka1-3',
                    'OstanovkaSHD',
                    'Park1-3',
                    'ParkSHD',
                    'Prem1',
                    'Prem2',
                    'Prem3',
                    'Prem4',
                    'Prem5',
                    'Prem6',
                    'Raion',
                    'Rassrochka',
                    'Region',
                    'Rinok1-3',
                    'RinokSHD',
                    'Sadik1-3',
                    'SadikSHD',
                    'Sells',
                    'Shkola',
                    'Shkola1-3',
                    'ShkolaSHD',
                    'Sport1-3',
                    'SportPloshadka',
                    'SportSHD',
                    'SposobOtotbrosheniya',
                    'Stadion',
                    'Top3Shka',
                    'VideoNabl',
                    'ZonaOtdih',
                    'PromoName',
                    'ID Apartment',
                    'maps',
                    'IM1',
                    'IM2',
                    'IM3',
                    'IM4',
                    'IM5',
                    'IM6',
                    'IM7',
                    'IM8',
                    'IM9',
                    'IM10',
                    'Action1',
                    'Action2',
                    'Action3',
                    'Action4',
                    'Action5',
                    'DrugieObekt1_3',
                    'Giper1_3',
                    'IDSegment',
                    'Magazin1_3',
                    'Ostanovka1_3',
                    'Park1_3',
                    'Rinok1_3',
                    'Sadik1_3',
                    'Shkola1_3',
                    'Sport1_3',
                    'IDApartment',
                ],
                'string',
            ],
            [['DateA', 'DateExpireid'], 'safe'],
            [['PriceDo', 'PriceOt', 'm2Do', 'm2Ot'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'AdressShka' => Yii::t('app', 'Adress Shka'),
            'Banks' => Yii::t('app', 'Banks'),
            'City' => Yii::t('app', 'City'),
            'DateA' => Yii::t('app', 'Date A'),
            'DateExpireid' => Yii::t('app', 'Date Expireid'),
            'DetskiePloshadki' => Yii::t('app', 'Detskie Ploshadki'),
            'DetskiySad' => Yii::t('app', 'Detskiy Sad'),
            'DrugieObekt1-3' => Yii::t('app', 'Drugie Obekt1 3'),
            'DrugieObekt1_3' => Yii::t('app', 'Drugie Obekt1 3'),
            'DrugieObektSHD' => Yii::t('app', 'Drugie Obekt Shd'),
            'Drugoe' => Yii::t('app', 'Drugoe'),
            'Etash' => Yii::t('app', 'Etash'),
            'Giper1-3' => Yii::t('app', 'Giper1 3'),
            'Giper1_3' => Yii::t('app', 'Giper1 3'),
            'GiperSHD' => Yii::t('app', 'Giper Shd'),
            'GostParking' => Yii::t('app', 'Gost Parking'),
            'ID Segment' => Yii::t('app', 'Id Segment'),
            'IDSegment' => Yii::t('app', 'Id Segment'),
            'IdZastroi' => Yii::t('app', 'Id Zastroi'),
            'KolLiter' => Yii::t('app', 'Kol Liter'),
            'Magazin1-3' => Yii::t('app', 'Magazin1 3'),
            'Magazin1_3' => Yii::t('app', 'Magazin1 3'),
            'MagazinSHD' => Yii::t('app', 'Magazin Shd'),
            'Magazine' => Yii::t('app', 'Magazine'),
            'NameShka' => Yii::t('app', 'Name Shka'),
            'NameShka2' => Yii::t('app', 'Name Shka2'),
            'NumberHouse' => Yii::t('app', 'Number House'),
            'OfficeYpravCompany' => Yii::t('app', 'Office Yprav Company'),
            'Ohrana' => Yii::t('app', 'Ohrana'),
            'OpisanieShka' => Yii::t('app', 'Opisanie Shka'),
            'Oplata' => Yii::t('app', 'Oplata'),
            'Ostanovka1-3' => Yii::t('app', 'Ostanovka1 3'),
            'Ostanovka1_3' => Yii::t('app', 'Ostanovka1 3'),
            'OstanovkaSHD' => Yii::t('app', 'Ostanovka Shd'),
            'Park1-3' => Yii::t('app', 'Park1 3'),
            'Park1_3' => Yii::t('app', 'Park1 3'),
            'ParkSHD' => Yii::t('app', 'Park Shd'),
            'Prem1' => Yii::t('app', 'Prem1'),
            'Prem2' => Yii::t('app', 'Prem2'),
            'Prem3' => Yii::t('app', 'Prem3'),
            'Prem4' => Yii::t('app', 'Prem4'),
            'Prem5' => Yii::t('app', 'Prem5'),
            'Prem6' => Yii::t('app', 'Prem6'),
            'PriceDo' => Yii::t('app', 'Price Do'),
            'PriceOt' => Yii::t('app', 'Price Ot'),
            'Raion' => Yii::t('app', 'Raion'),
            'Rassrochka' => Yii::t('app', 'Rassrochka'),
            'Region' => Yii::t('app', 'Region'),
            'Rinok1-3' => Yii::t('app', 'Rinok1 3'),
            'Rinok1_3' => Yii::t('app', 'Rinok1 3'),
            'RinokSHD' => Yii::t('app', 'Rinok Shd'),
            'Sadik1-3' => Yii::t('app', 'Sadik1 3'),
            'Sadik1_3' => Yii::t('app', 'Sadik1 3'),
            'SadikSHD' => Yii::t('app', 'Sadik Shd'),
            'Sells' => Yii::t('app', 'Sells'),
            'Shkola' => Yii::t('app', 'Shkola'),
            'Shkola1-3' => Yii::t('app', 'Shkola1 3'),
            'Shkola1_3' => Yii::t('app', 'Shkola1 3'),
            'ShkolaSHD' => Yii::t('app', 'Shkola Shd'),
            'Sport1-3' => Yii::t('app', 'Sport1 3'),
            'Sport1_3' => Yii::t('app', 'Sport1 3'),
            'SportPloshadka' => Yii::t('app', 'Sport Ploshadka'),
            'SportSHD' => Yii::t('app', 'Sport Shd'),
            'SposobOtotbrosheniya' => Yii::t('app', 'Sposob Ototbrosheniya'),
            'Stadion' => Yii::t('app', 'Stadion'),
            'Top3Shka' => Yii::t('app', 'Top3 Shka'),
            'VideoNabl' => Yii::t('app', 'Video Nabl'),
            'ZonaOtdih' => Yii::t('app', 'Zona Otdih'),
            'm2Do' => Yii::t('app', 'M2 Do'),
            'm2Ot' => Yii::t('app', 'M2 Ot'),
            'PromoName' => Yii::t('app', 'Promo Name'),
            'ID Apartment' => Yii::t('app', 'Id Apartment'),
            'IDApartment' => Yii::t('app', 'Id Apartment'),
            'maps' => Yii::t('app', 'Maps'),
            'IM1' => Yii::t('app', 'Im1'),
            'IM2' => Yii::t('app', 'Im2'),
            'IM3' => Yii::t('app', 'Im3'),
            'IM4' => Yii::t('app', 'Im4'),
            'IM5' => Yii::t('app', 'Im5'),
            'IM6' => Yii::t('app', 'Im6'),
            'IM7' => Yii::t('app', 'Im7'),
            'IM8' => Yii::t('app', 'Im8'),
            'IM9' => Yii::t('app', 'Im9'),
            'IM10' => Yii::t('app', 'Im10'),
            'Action1' => Yii::t('app', 'Action1'),
            'Action2' => Yii::t('app', 'Action2'),
            'Action3' => Yii::t('app', 'Action3'),
            'Action4' => Yii::t('app', 'Action4'),
            'Action5' => Yii::t('app', 'Action5'),
        ];
    }
}
