<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "apartment".
 *
 * @property int $id
 * @property string $key
 * @property string|null $BalkonLodgiya
 * @property string|null $DopOption
 * @property string|null $DopOtdelka
 * @property string|null $DveriVhod
 * @property string|null $Electro
 * @property string|null $IDHouse
 * @property string|null $IDShka
 * @property string|null $IM1
 * @property string|null $IM2
 * @property string|null $IM3
 * @property string|null $IM4
 * @property string|null $IM5
 * @property string|null $IM6
 * @property string|null $IM7
 * @property string|null $IM8
 * @property string|null $IM9
 * @property string|null $IM10
 * @property string|null $KolKomnat
 * @property string|null $Okna
 * @property string|null $OpisanieKvartiri
 * @property string|null $OsteklenieLodgii
 * @property string|null $OsteklenieOkna
 * @property string|null $Otoplenie
 * @property string|null $Otopleniye
 * @property string|null $Peregorodki
 * @property string|null $Poshar
 * @property string|null $Prem1
 * @property string|null $Prem2
 * @property string|null $Prem3
 * @property string|null $Prem4
 * @property string|null $Prem5
 * @property string|null $Prem6
 * @property string|null $Santehnika
 * @property string|null $Schetchik
 * @property string|null $SposobOtobrasheniya
 * @property string|null $StoronaSveta
 * @property string|null $Vid
 * @property string|null $Vitrash
 * @property string|null $m2balkon
 * @property string|null $m2kuhna
 * @property string|null $m2lodshiya
 * @property string|null $m2obshaya
 * @property string|null $m2shilaya
 * @property string|null $visotaPotolka
 * @property int $enable
 */
class Apartment extends \yii\db\ActiveRecord
{
    public $IDHouse;
    public  $IDShka;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apartment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['key', 'BalkonLodgiya', 'DopOption', 'DopOtdelka', 'DveriVhod', 'Electro', 'ID House', 'ID Shka',  'IDHouse', 'IDShka', 'IM1', 'IM2', 'IM3', 'IM4', 'IM5', 'IM6', 'IM7', 'IM8', 'IM9', 'IM10', 'KolKomnat', 'Okna', 'OpisanieKvartiri', 'OsteklenieLodgii', 'OsteklenieOkna', 'Otoplenie', 'Otopleniye', 'Peregorodki', 'Poshar', 'Prem1', 'Prem2', 'Prem3', 'Prem4', 'Prem5', 'Prem6', 'Santehnika', 'Schetchik', 'SposobOtobrasheniya', 'StoronaSveta', 'Vid', 'Vitrash', 'm2balkon', 'm2kuhna', 'm2lodshiya', 'm2obshaya', 'm2shilaya', 'visotaPotolka'], 'string'],
            [['enable'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'BalkonLodgiya' => Yii::t('app', 'Balkon Lodgiya'),
            'DopOption' => Yii::t('app', 'Dop Option'),
            'DopOtdelka' => Yii::t('app', 'Dop Otdelka'),
            'DveriVhod' => Yii::t('app', 'Dveri Vhod'),
            'Electro' => Yii::t('app', 'Electro'),
            'ID House' => Yii::t('app', 'Id House'),
            'IDHouse' => Yii::t('app', 'Id House'),
            'ID Shka' => Yii::t('app', 'Id Shka'),
            'IDShka' => Yii::t('app', 'Id Shka'),
            'IM1' => Yii::t('app', 'Im1'),
            'IM2' => Yii::t('app', 'Im2'),
            'IM3' => Yii::t('app', 'Im3'),
            'IM4' => Yii::t('app', 'Im4'),
            'IM5' => Yii::t('app', 'Im5'),
            'IM6' => Yii::t('app', 'Im6'),
            'IM7' => Yii::t('app', 'Im7'),
            'IM8' => Yii::t('app', 'Im8'),
            'IM9' => Yii::t('app', 'Im9'),
            'IM10' => Yii::t('app', 'Im10'),
            'KolKomnat' => Yii::t('app', 'Kol Komnat'),
            'Okna' => Yii::t('app', 'Okna'),
            'OpisanieKvartiri' => Yii::t('app', 'Opisanie Kvartiri'),
            'OsteklenieLodgii' => Yii::t('app', 'Osteklenie Lodgii'),
            'OsteklenieOkna' => Yii::t('app', 'Osteklenie Okna'),
            'Otoplenie' => Yii::t('app', 'Otoplenie'),
            'Otopleniye' => Yii::t('app', 'Otopleniye'),
            'Peregorodki' => Yii::t('app', 'Peregorodki'),
            'Poshar' => Yii::t('app', 'Poshar'),
            'Prem1' => Yii::t('app', 'Prem1'),
            'Prem2' => Yii::t('app', 'Prem2'),
            'Prem3' => Yii::t('app', 'Prem3'),
            'Prem4' => Yii::t('app', 'Prem4'),
            'Prem5' => Yii::t('app', 'Prem5'),
            'Prem6' => Yii::t('app', 'Prem6'),
            'Santehnika' => Yii::t('app', 'Santehnika'),
            'Schetchik' => Yii::t('app', 'Schetchik'),
            'SposobOtobrasheniya' => Yii::t('app', 'Sposob Otobrasheniya'),
            'StoronaSveta' => Yii::t('app', 'Storona Sveta'),
            'Vid' => Yii::t('app', 'Vid'),
            'Vitrash' => Yii::t('app', 'Vitrash'),
            'm2balkon' => Yii::t('app', 'M2balkon'),
            'm2kuhna' => Yii::t('app', 'M2kuhna'),
            'm2lodshiya' => Yii::t('app', 'M2lodshiya'),
            'm2obshaya' => Yii::t('app', 'M2obshaya'),
            'm2shilaya' => Yii::t('app', 'M2shilaya'),
            'visotaPotolka' => Yii::t('app', 'Visota Potolka'),
            'enable' => Yii::t('app', 'Enable'),
        ];
    }
}
