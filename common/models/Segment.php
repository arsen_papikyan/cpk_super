<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "segment".
 *
 * @property int $id
 * @property string $key
 * @property string $NameSegment
 */
class Segment extends \yii\db\ActiveRecord
{
    public $NameSegment;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'segment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'Name Segment', 'NameSegment'], 'required'],
            [['key', 'Name Segment', 'NameSegment'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'Name Segment' => Yii::t('app', 'Name Segment'),
            'NameSegment' => Yii::t('app', 'Name Segment'),
        ];
    }
}
