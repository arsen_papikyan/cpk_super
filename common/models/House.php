<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "house".
 *
 * @property int $id
 * @property string|null $key
 * @property string|null $DopZona
 * @property string|null $Etash
 * @property string|null $Gaz
 * @property string|null $IDShka
 * @property string|null $KolKvartir
 * @property string|null $KolLift
 * @property string|null $KolPodezd
 * @property string|null $Konsersh
 * @property string|null $Material
 * @property string|null $OtdelkaZon
 * @property string|null $Otoplenie
 * @property string|null $PodzemnayaParking
 * @property string|null $Shouroom
 * @property string|null $Territory
 */
class House extends \yii\db\ActiveRecord
{
    public $IDShka;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'house';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'key',
                    'DopZona',
                    'Etash',
                    'Gaz',
                    'ID Shka',
                    'IDShka',
                    'KolKvartir',
                    'KolLift',
                    'KolPodezd',
                    'Konsersh',
                    'Material',
                    'OtdelkaZon',
                    'Otoplenie',
                    'PodzemnayaParking',
                    'Shouroom',
                    'Territory',
                ],
                'string',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'DopZona' => Yii::t('app', 'Dop Zona'),
            'Etash' => Yii::t('app', 'Etash'),
            'Gaz' => Yii::t('app', 'Gaz'),
            'ID Shka' => Yii::t('app', 'Id Shka'),
            'IDShka' => Yii::t('app', 'Id Shka'),
            'KolKvartir' => Yii::t('app', 'Kol Kvartir'),
            'KolLift' => Yii::t('app', 'Kol Lift'),
            'KolPodezd' => Yii::t('app', 'Kol Podezd'),
            'Konsersh' => Yii::t('app', 'Konsersh'),
            'Material' => Yii::t('app', 'Material'),
            'OtdelkaZon' => Yii::t('app', 'Otdelka Zon'),
            'Otoplenie' => Yii::t('app', 'Otoplenie'),
            'PodzemnayaParking' => Yii::t('app', 'Podzemnaya Parking'),
            'Shouroom' => Yii::t('app', 'Shouroom'),
            'Territory' => Yii::t('app', 'Territory'),
        ];
    }
}
