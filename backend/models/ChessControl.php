<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Chess;

/**
 * ChessControl represents the model behind the search form of `common\models\Chess`.
 */
class ChessControl extends Chess
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'Etash', 'Price2', 'Price3', 'PriceBase'], 'integer'],
            [
                [
                    'key',
                    'DataSdachi',
                    'DateActual',
                    'ID Apartment',
                    'ID House',
                    'IDHouse',
                    'ID Shka',
                    'IDShka',
                    'NameShaka',
                    'Number',
                    'Podezd',
                    'SposobOtobrasheniya',
                    'Status',
                    'Top3kv',
                ],
                'trim',
            ],
            [['m2obshaya'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Chess::find()->orderBy(['id'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'DataSdachi' => $this->DataSdachi,
            'DateActual' => $this->DateActual,
            'Etash' => $this->Etash,
            'Price2' => $this->Price2,
            'Price3' => $this->Price3,
            'PriceBase' => $this->PriceBase,
            'm2obshaya' => $this->m2obshaya,
        ]);

        $query->andFilterWhere(['like', 'key', $this->key])

            ->andFilterWhere(['like', 'NameShaka', $this->NameShaka])
            ->andFilterWhere(['like', 'Number', $this->Number])
            ->andFilterWhere(['like', 'Podezd', $this->Podezd])
            ->andFilterWhere(['like', 'SposobOtobrasheniya', $this->SposobOtobrasheniya])
            ->andFilterWhere(['like', 'Status', $this->Status])
            ->andFilterWhere(['like', 'Top3kv', $this->Top3kv])
        ;

        return $dataProvider;
    }
}
