<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\House;

/**
 * HouseControl represents the model behind the search form of `common\models\House`.
 */
class HouseControl extends House
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['key', 'DopZona', 'Etash', 'Gaz', 'ID Shka',  'IDShka', 'KolKvartir', 'KolLift', 'KolPodezd', 'Konsersh', 'Material', 'OtdelkaZon', 'Otoplenie', 'PodzemnayaParking', 'Shouroom', 'Territory'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = House::find()->orderBy(['id'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'DopZona', $this->DopZona])
            ->andFilterWhere(['like', 'Etash', $this->Etash])
            ->andFilterWhere(['like', 'Gaz', $this->Gaz])
            ->andFilterWhere(['like', 'ID Shka', $this['ID Shka']])
            ->andFilterWhere(['like', 'KolKvartir', $this->KolKvartir])
            ->andFilterWhere(['like', 'KolLift', $this->KolLift])
            ->andFilterWhere(['like', 'KolPodezd', $this->KolPodezd])
            ->andFilterWhere(['like', 'Konsersh', $this->Konsersh])
            ->andFilterWhere(['like', 'Material', $this->Material])
            ->andFilterWhere(['like', 'OtdelkaZon', $this->OtdelkaZon])
            ->andFilterWhere(['like', 'Otoplenie', $this->Otoplenie])
            ->andFilterWhere(['like', 'PodzemnayaParking', $this->PodzemnayaParking])
            ->andFilterWhere(['like', 'Shouroom', $this->Shouroom])
            ->andFilterWhere(['like', 'Territory', $this->Territory]);

        return $dataProvider;
    }
}
