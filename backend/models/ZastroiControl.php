<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Zastroi;

/**
 * ZastroiControl represents the model behind the search form of `common\models\Zastroi`.
 */
class ZastroiControl extends Zastroi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['key', 'AdressZ', 'DateActual', 'DocZ', 'NameZ', 'NameZ2', 'OfficeZ', 'OsnovanieZ', 'SdannieObgectZ', 'SiteZ', 'WatchJobZ', 'YpravZ'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zastroi::find()->orderBy(['id'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'DateActual' => $this->DateActual,
        ]);

        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'AdressZ', $this->AdressZ])
            ->andFilterWhere(['like', 'DocZ', $this->DocZ])
            ->andFilterWhere(['like', 'NameZ', $this->NameZ])
            ->andFilterWhere(['like', 'NameZ2', $this->NameZ2])
            ->andFilterWhere(['like', 'OfficeZ', $this->OfficeZ])
            ->andFilterWhere(['like', 'OsnovanieZ', $this->OsnovanieZ])
            ->andFilterWhere(['like', 'SdannieObgectZ', $this->SdannieObgectZ])
            ->andFilterWhere(['like', 'SiteZ', $this->SiteZ])
            ->andFilterWhere(['like', 'WatchJobZ', $this->WatchJobZ])
            ->andFilterWhere(['like', 'YpravZ', $this->YpravZ]);

        return $dataProvider;
    }
}
