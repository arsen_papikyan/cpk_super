<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Shka;

/**
 * ShkaControl represents the model behind the search form of `common\models\Shka`.
 */
class ShkaControl extends Shka
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'PriceDo', 'PriceOt', 'm2Do', 'm2Ot'], 'integer'],
            [['key', 'AdressShka', 'Banks', 'City', 'DateA', 'DateExpireid', 'DetskiePloshadki', 'DetskiySad', 'DrugieObekt1-3', 'DrugieObektSHD', 'Drugoe', 'Etash', 'Giper1-3', 'GiperSHD', 'GostParking', 'ID Segment', 'IdZastroi', 'KolLiter', 'Magazin1-3', 'MagazinSHD', 'Magazine', 'NameShka', 'NameShka2', 'NumberHouse', 'OfficeYpravCompany', 'Ohrana', 'OpisanieShka', 'Oplata', 'Ostanovka1-3', 'OstanovkaSHD', 'Park1-3', 'ParkSHD', 'Prem1', 'Prem2', 'Prem3', 'Prem4', 'Prem5', 'Prem6', 'Raion', 'Rassrochka', 'Region', 'Rinok1-3', 'RinokSHD', 'Sadik1-3', 'SadikSHD', 'Sells', 'Shkola', 'Shkola1-3', 'ShkolaSHD', 'Sport1-3', 'SportPloshadka', 'SportSHD', 'SposobOtotbrosheniya', 'Stadion', 'Top3Shka', 'VideoNabl', 'ZonaOtdih', 'PromoName', 'ID Apartment', 'maps', 'IM1', 'IM2', 'IM3', 'IM4', 'IM5', 'IM6', 'IM7', 'IM8', 'IM9', 'IM10', 'Action1', 'Action2', 'Action3', 'Action4', 'Action5'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Shka::find()->orderBy(['id'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'DateA' => $this->DateA,
            'DateExpireid' => $this->DateExpireid,
            'PriceDo' => $this->PriceDo,
            'PriceOt' => $this->PriceOt,
            'm2Do' => $this->m2Do,
            'm2Ot' => $this->m2Ot,
        ]);

        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'AdressShka', $this->AdressShka])
            ->andFilterWhere(['like', 'Banks', $this->Banks])
            ->andFilterWhere(['like', 'City', $this->City])
            ->andFilterWhere(['like', 'DetskiePloshadki', $this->DetskiePloshadki])
            ->andFilterWhere(['like', 'DetskiySad', $this->DetskiySad])
            ->andFilterWhere(['like', 'DrugieObektSHD', $this->DrugieObektSHD])
            ->andFilterWhere(['like', 'Drugoe', $this->Drugoe])
            ->andFilterWhere(['like', 'Etash', $this->Etash])
            ->andFilterWhere(['like', 'GiperSHD', $this->GiperSHD])
            ->andFilterWhere(['like', 'GostParking', $this->GostParking])
            ->andFilterWhere(['like', 'IdZastroi', $this->IdZastroi])
            ->andFilterWhere(['like', 'KolLiter', $this->KolLiter])
            ->andFilterWhere(['like', 'MagazinSHD', $this->MagazinSHD])
            ->andFilterWhere(['like', 'Magazine', $this->Magazine])
            ->andFilterWhere(['like', 'NameShka', $this->NameShka])
            ->andFilterWhere(['like', 'NameShka2', $this->NameShka2])
            ->andFilterWhere(['like', 'NumberHouse', $this->NumberHouse])
            ->andFilterWhere(['like', 'OfficeYpravCompany', $this->OfficeYpravCompany])
            ->andFilterWhere(['like', 'Ohrana', $this->Ohrana])
            ->andFilterWhere(['like', 'OpisanieShka', $this->OpisanieShka])
            ->andFilterWhere(['like', 'Oplata', $this->Oplata])
            ->andFilterWhere(['like', 'OstanovkaSHD', $this->OstanovkaSHD])
            ->andFilterWhere(['like', 'ParkSHD', $this->ParkSHD])
            ->andFilterWhere(['like', 'Prem1', $this->Prem1])
            ->andFilterWhere(['like', 'Prem2', $this->Prem2])
            ->andFilterWhere(['like', 'Prem3', $this->Prem3])
            ->andFilterWhere(['like', 'Prem4', $this->Prem4])
            ->andFilterWhere(['like', 'Prem5', $this->Prem5])
            ->andFilterWhere(['like', 'Prem6', $this->Prem6])
            ->andFilterWhere(['like', 'Raion', $this->Raion])
            ->andFilterWhere(['like', 'Rassrochka', $this->Rassrochka])
            ->andFilterWhere(['like', 'Region', $this->Region])
            ->andFilterWhere(['like', 'RinokSHD', $this->RinokSHD])
            ->andFilterWhere(['like', 'SadikSHD', $this->SadikSHD])
            ->andFilterWhere(['like', 'Sells', $this->Sells])
            ->andFilterWhere(['like', 'Shkola', $this->Shkola])
            ->andFilterWhere(['like', 'ShkolaSHD', $this->ShkolaSHD])
            ->andFilterWhere(['like', 'SportPloshadka', $this->SportPloshadka])
            ->andFilterWhere(['like', 'SportSHD', $this->SportSHD])
            ->andFilterWhere(['like', 'SposobOtotbrosheniya', $this->SposobOtotbrosheniya])
            ->andFilterWhere(['like', 'Stadion', $this->Stadion])
            ->andFilterWhere(['like', 'Top3Shka', $this->Top3Shka])
            ->andFilterWhere(['like', 'VideoNabl', $this->VideoNabl])
            ->andFilterWhere(['like', 'ZonaOtdih', $this->ZonaOtdih])
            ->andFilterWhere(['like', 'PromoName', $this->PromoName])
            ->andFilterWhere(['like', 'ID Apartment', $this['ID Apartment']])
            ->andFilterWhere(['like', 'maps', $this->maps])
            ->andFilterWhere(['like', 'IM1', $this->IM1])
            ->andFilterWhere(['like', 'IM2', $this->IM2])
            ->andFilterWhere(['like', 'IM3', $this->IM3])
            ->andFilterWhere(['like', 'IM4', $this->IM4])
            ->andFilterWhere(['like', 'IM5', $this->IM5])
            ->andFilterWhere(['like', 'IM6', $this->IM6])
            ->andFilterWhere(['like', 'IM7', $this->IM7])
            ->andFilterWhere(['like', 'IM8', $this->IM8])
            ->andFilterWhere(['like', 'IM9', $this->IM9])
            ->andFilterWhere(['like', 'IM10', $this->IM10])
            ->andFilterWhere(['like', 'Action1', $this->Action1])
            ->andFilterWhere(['like', 'Action2', $this->Action2])
            ->andFilterWhere(['like', 'Action3', $this->Action3])
            ->andFilterWhere(['like', 'Action4', $this->Action4])
            ->andFilterWhere(['like', 'Action5', $this->Action5]);

        return $dataProvider;
    }
}
