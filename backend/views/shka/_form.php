<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Shka */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shka-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'key')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'AdressShka')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Banks')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'City')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Action5')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'DateExpireid')->widget(
                    \kartik\date\DatePicker::class,
                    [
                        'name' => 'DateActual',
                        'type' => \kartik\date\DatePicker::TYPE_COMPONENT_PREPEND,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                        ],
                    ]
                )  ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'DetskiePloshadki')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'DetskiySad')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'DrugieObekt1_3')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'DrugieObektSHD')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Drugoe')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Etash')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Giper1_3')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'GiperSHD')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'GostParking')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IDSegment')->widget(
                    \kartik\select2\Select2::classname(), [
                    'data' =>
                        \yii\helpers\ArrayHelper::map(
                            \common\models\Segment::find()
                                ->select(['id, `key`,  `Name Segment` name'])
                                ->asArray()
                                ->all(),
                            "key", "name"),
                    'options' => ['placeholder' => 'Parent'],
                    'pluginOptions' => ['allowClear' => true],
                ])?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IdZastroi')->widget(
                    \kartik\select2\Select2::classname(), [
                    'data' =>
                        \yii\helpers\ArrayHelper::map(
                            \common\models\Zastroi::find()
                                ->select('id,key,NameZ')
                                ->asArray()
                                ->all(),
                            "key", "NameZ"),
                    'options' => ['placeholder' => 'Parent'],
                    'pluginOptions' => ['allowClear' => true],
                ])?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'KolLiter')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Magazin1_3')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'MagazinSHD')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Magazine')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'NameShka')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'NameShka2')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'NumberHouse')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'OfficeYpravCompany')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Ohrana')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'OpisanieShka')->textarea(['row'=>3]) ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Oplata')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Ostanovka1_3')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'OstanovkaSHD')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Park1_3')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'ParkSHD')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Prem1')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Prem2')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Prem3')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Prem4')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Prem5')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Prem6')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'PriceDo')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'PriceOt')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Raion')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Rassrochka')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Region')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Rinok1_3')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'RinokSHD')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Sadik1_3')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'SadikSHD')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Sells')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Shkola')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Shkola1_3')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'ShkolaSHD')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Sport1_3')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'SportPloshadka')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'SportSHD')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'SposobOtotbrosheniya')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Stadion')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Top3Shka')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'VideoNabl')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'ZonaOtdih')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'm2Do')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'm2Ot')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'PromoName')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IDApartment')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'maps')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IM1')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IM2')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IM3')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IM4')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IM5')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IM6')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IM7')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IM8')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IM9')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IM10')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Action1')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Action2')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Action3')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Action4')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
