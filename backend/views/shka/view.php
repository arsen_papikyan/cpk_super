<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Shka */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shkas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="shka-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'key:ntext',
            'AdressShka:ntext',
            'Banks:ntext',
            'City:ntext',
            'DateA',
            'DateExpireid',
            'DetskiePloshadki:ntext',
            'DetskiySad:ntext',
            'DrugieObekt1-3:ntext',
            'DrugieObektSHD:ntext',
            'Drugoe:ntext',
            'Etash:ntext',
            'Giper1-3:ntext',
            'GiperSHD:ntext',
            'GostParking:ntext',
            'ID Segment:ntext',
            'IdZastroi:ntext',
            'KolLiter:ntext',
            'Magazin1-3:ntext',
            'MagazinSHD:ntext',
            'Magazine:ntext',
            'NameShka:ntext',
            'NameShka2:ntext',
            'NumberHouse:ntext',
            'OfficeYpravCompany:ntext',
            'Ohrana:ntext',
            'OpisanieShka:ntext',
            'Oplata:ntext',
            'Ostanovka1-3:ntext',
            'OstanovkaSHD:ntext',
            'Park1-3:ntext',
            'ParkSHD:ntext',
            'Prem1:ntext',
            'Prem2:ntext',
            'Prem3:ntext',
            'Prem4:ntext',
            'Prem5:ntext',
            'Prem6:ntext',
            'PriceDo',
            'PriceOt',
            'Raion:ntext',
            'Rassrochka:ntext',
            'Region:ntext',
            'Rinok1-3:ntext',
            'RinokSHD:ntext',
            'Sadik1-3:ntext',
            'SadikSHD:ntext',
            'Sells:ntext',
            'Shkola:ntext',
            'Shkola1-3:ntext',
            'ShkolaSHD:ntext',
            'Sport1-3:ntext',
            'SportPloshadka:ntext',
            'SportSHD:ntext',
            'SposobOtotbrosheniya:ntext',
            'Stadion:ntext',
            'Top3Shka:ntext',
            'VideoNabl:ntext',
            'ZonaOtdih:ntext',
            'm2Do',
            'm2Ot',
            'PromoName:ntext',
            'ID Apartment:ntext',
            'maps:ntext',
            'IM1:ntext',
            'IM2:ntext',
            'IM3:ntext',
            'IM4:ntext',
            'IM5:ntext',
            'IM6:ntext',
            'IM7:ntext',
            'IM8:ntext',
            'IM9:ntext',
            'IM10:ntext',
            'Action1:ntext',
            'Action2:ntext',
            'Action3:ntext',
            'Action4:ntext',
            'Action5:ntext',
        ],
    ]) ?>

</div>
