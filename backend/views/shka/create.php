<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Shka */

$this->title = Yii::t('app', 'Create Shka');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shkas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shka-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
