<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ShkaControl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shka-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'key') ?>

    <?= $form->field($model, 'AdressShka') ?>

    <?= $form->field($model, 'Banks') ?>

    <?= $form->field($model, 'City') ?>

    <?php // echo $form->field($model, 'DateA') ?>

    <?php // echo $form->field($model, 'DateExpireid') ?>

    <?php // echo $form->field($model, 'DetskiePloshadki') ?>

    <?php // echo $form->field($model, 'DetskiySad') ?>

    <?php // echo $form->field($model, 'DrugieObekt1-3') ?>

    <?php // echo $form->field($model, 'DrugieObektSHD') ?>

    <?php // echo $form->field($model, 'Drugoe') ?>

    <?php // echo $form->field($model, 'Etash') ?>

    <?php // echo $form->field($model, 'Giper1-3') ?>

    <?php // echo $form->field($model, 'GiperSHD') ?>

    <?php // echo $form->field($model, 'GostParking') ?>

    <?php // echo $form->field($model, 'ID Segment') ?>

    <?php // echo $form->field($model, 'IdZastroi') ?>

    <?php // echo $form->field($model, 'KolLiter') ?>

    <?php // echo $form->field($model, 'Magazin1-3') ?>

    <?php // echo $form->field($model, 'MagazinSHD') ?>

    <?php // echo $form->field($model, 'Magazine') ?>

    <?php // echo $form->field($model, 'NameShka') ?>

    <?php // echo $form->field($model, 'NameShka2') ?>

    <?php // echo $form->field($model, 'NumberHouse') ?>

    <?php // echo $form->field($model, 'OfficeYpravCompany') ?>

    <?php // echo $form->field($model, 'Ohrana') ?>

    <?php // echo $form->field($model, 'OpisanieShka') ?>

    <?php // echo $form->field($model, 'Oplata') ?>

    <?php // echo $form->field($model, 'Ostanovka1-3') ?>

    <?php // echo $form->field($model, 'OstanovkaSHD') ?>

    <?php // echo $form->field($model, 'Park1-3') ?>

    <?php // echo $form->field($model, 'ParkSHD') ?>

    <?php // echo $form->field($model, 'Prem1') ?>

    <?php // echo $form->field($model, 'Prem2') ?>

    <?php // echo $form->field($model, 'Prem3') ?>

    <?php // echo $form->field($model, 'Prem4') ?>

    <?php // echo $form->field($model, 'Prem5') ?>

    <?php // echo $form->field($model, 'Prem6') ?>

    <?php // echo $form->field($model, 'PriceDo') ?>

    <?php // echo $form->field($model, 'PriceOt') ?>

    <?php // echo $form->field($model, 'Raion') ?>

    <?php // echo $form->field($model, 'Rassrochka') ?>

    <?php // echo $form->field($model, 'Region') ?>

    <?php // echo $form->field($model, 'Rinok1-3') ?>

    <?php // echo $form->field($model, 'RinokSHD') ?>

    <?php // echo $form->field($model, 'Sadik1-3') ?>

    <?php // echo $form->field($model, 'SadikSHD') ?>

    <?php // echo $form->field($model, 'Sells') ?>

    <?php // echo $form->field($model, 'Shkola') ?>

    <?php // echo $form->field($model, 'Shkola1-3') ?>

    <?php // echo $form->field($model, 'ShkolaSHD') ?>

    <?php // echo $form->field($model, 'Sport1-3') ?>

    <?php // echo $form->field($model, 'SportPloshadka') ?>

    <?php // echo $form->field($model, 'SportSHD') ?>

    <?php // echo $form->field($model, 'SposobOtotbrosheniya') ?>

    <?php // echo $form->field($model, 'Stadion') ?>

    <?php // echo $form->field($model, 'Top3Shka') ?>

    <?php // echo $form->field($model, 'VideoNabl') ?>

    <?php // echo $form->field($model, 'ZonaOtdih') ?>

    <?php // echo $form->field($model, 'm2Do') ?>

    <?php // echo $form->field($model, 'm2Ot') ?>

    <?php // echo $form->field($model, 'PromoName') ?>

    <?php // echo $form->field($model, 'ID Apartment') ?>

    <?php // echo $form->field($model, 'maps') ?>

    <?php // echo $form->field($model, 'IM1') ?>

    <?php // echo $form->field($model, 'IM2') ?>

    <?php // echo $form->field($model, 'IM3') ?>

    <?php // echo $form->field($model, 'IM4') ?>

    <?php // echo $form->field($model, 'IM5') ?>

    <?php // echo $form->field($model, 'IM6') ?>

    <?php // echo $form->field($model, 'IM7') ?>

    <?php // echo $form->field($model, 'IM8') ?>

    <?php // echo $form->field($model, 'IM9') ?>

    <?php // echo $form->field($model, 'IM10') ?>

    <?php // echo $form->field($model, 'Action1') ?>

    <?php // echo $form->field($model, 'Action2') ?>

    <?php // echo $form->field($model, 'Action3') ?>

    <?php // echo $form->field($model, 'Action4') ?>

    <?php // echo $form->field($model, 'Action5') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
