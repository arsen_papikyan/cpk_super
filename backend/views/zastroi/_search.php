<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ZastroiControl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zastroi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'key') ?>

    <?= $form->field($model, 'AdressZ') ?>

    <?= $form->field($model, 'DateActual') ?>

    <?= $form->field($model, 'DocZ') ?>

    <?php // echo $form->field($model, 'NameZ') ?>

    <?php // echo $form->field($model, 'NameZ2') ?>

    <?php // echo $form->field($model, 'OfficeZ') ?>

    <?php // echo $form->field($model, 'OsnovanieZ') ?>

    <?php // echo $form->field($model, 'SdannieObgectZ') ?>

    <?php // echo $form->field($model, 'SiteZ') ?>

    <?php // echo $form->field($model, 'WatchJobZ') ?>

    <?php // echo $form->field($model, 'YpravZ') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
