<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Zastroi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zastroi-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'key')->textInput()?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'AdressZ')->textInput()?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'NameZ')->textInput()?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'NameZ2')->textInput()?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'OfficeZ')->textInput()?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'OsnovanieZ')->textInput()?>
            </div>
        </div>
    </div>



    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'SdannieObgectZ')->textInput()?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'SiteZ')->textInput()?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'WatchJobZ')->textInput()?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'YpravZ')->textInput()?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <?= $form->field($model, 'DocZ')->textInput()?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
