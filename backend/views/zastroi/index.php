<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ZastroiControl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zastrois');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zastroi-index table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Zastroi'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'key:ntext',
            'NameZ:ntext',
            'AdressZ:ntext',
//            'DocZ:ntext',
//            'NameZ2:ntext',
            'OfficeZ:ntext',
//            'OsnovanieZ:ntext',
//            'SdannieObgectZ:ntext',
//            'SiteZ:ntext',
//            'WatchJobZ:ntext',
//            'YpravZ:ntext',
            'DateActual',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{duplicate}  {view} {update} {delete}',
                'buttons' =>
                    [
                        'duplicate' => function ($url, $model) {
                            return Html::a('<span  class="fa fa-files-o" aria-hidden="true""></span>', $url, [
                                'title' => Yii::t('app', 'Duplicate'),
                            ]);
                        },
                    ],
            ],
        ],
    ]); ?>


</div>
