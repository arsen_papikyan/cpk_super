<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SegmentControl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Segments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="segment-index table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Segment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'key:ntext',
//            'Name Segment:ntext',
            [
                'attribute' => 'NameSegment',
                'value' => function ($data) {
                    return $data['Name Segment'];
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{duplicate}  {view} {update} {delete}',
                'buttons' =>
                    [
                        'duplicate' => function ($url, $model) {
                            return Html::a('<span  class="fa fa-files-o" aria-hidden="true""></span>', $url, [
                                'title' => Yii::t('app', 'Duplicate'),
                            ]);
                        },
                    ],
            ],
        ],
    ]); ?>


</div>
