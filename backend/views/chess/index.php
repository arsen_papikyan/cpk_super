<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChessControl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Chesses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chess-index table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Chess'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'key:ntext',
            'Etash',
            //'ID Apartment:ntext',
            //'ID House:ntext',
            //'ID Shka:ntext',
            [
                'attribute' => 'IDShka',
                'value' => function ($data) {
                    return \common\models\Shka::findOne(['key' => $data['ID Shka']])->NameShka;
                },
            ],
            [
                'attribute' => 'IDHouse',
                'value' => function ($data) {
                    return $data['ID House'];
                },
            ],
            'NameShaka:ntext',
            'Number:ntext',
            'Podezd:ntext',
            'Price2',
            'Price3',
            'PriceBase',
            'SposobOtobrasheniya:ntext',
            'Status:ntext',
//            'Top3kv:ntext',
            'm2obshaya',
            'DataSdachi',
            'DateActual',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{duplicate}  {view} {update} {delete}',
                'buttons' =>
                    [
                        'duplicate' => function ($url, $model) {
                            return Html::a('<span  class="fa fa-files-o" aria-hidden="true""></span>', $url, [
                                'title' => Yii::t('app', 'Duplicate'),
                            ]);
                        },
                    ],
            ],
        ],
    ]); ?>


</div>
