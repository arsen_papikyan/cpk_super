<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Chess */

$this->title = Yii::t('app', 'Create Chess');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Chesses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chess-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
