<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Chess */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Chesses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="chess-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'key:ntext',
            'DataSdachi',
            'DateActual',
            'Etash',
            'ID Apartment:ntext',
            'ID House:ntext',
            'ID Shka:ntext',
            'NameShaka:ntext',
            'Number:ntext',
            'Podezd:ntext',
            'Price2',
            'Price3',
            'PriceBase',
            'SposobOtobrasheniya:ntext',
            'Status:ntext',
            'Top3kv:ntext',
            'm2obshaya',
        ],
    ]) ?>

</div>
