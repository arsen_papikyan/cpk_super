<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ChessControl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chess-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'key') ?>

    <?= $form->field($model, 'DataSdachi') ?>

    <?= $form->field($model, 'DateActual') ?>

    <?= $form->field($model, 'Etash') ?>

    <?php // echo $form->field($model, 'ID Apartment') ?>

    <?php // echo $form->field($model, 'ID House') ?>

    <?php // echo $form->field($model, 'ID Shka') ?>

    <?php // echo $form->field($model, 'NameShaka') ?>

    <?php // echo $form->field($model, 'Number') ?>

    <?php // echo $form->field($model, 'Podezd') ?>

    <?php // echo $form->field($model, 'Price2') ?>

    <?php // echo $form->field($model, 'Price3') ?>

    <?php // echo $form->field($model, 'PriceBase') ?>

    <?php // echo $form->field($model, 'SposobOtobrasheniya') ?>

    <?php // echo $form->field($model, 'Status') ?>

    <?php // echo $form->field($model, 'Top3kv') ?>

    <?php // echo $form->field($model, 'm2obshaya') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
