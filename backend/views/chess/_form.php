<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Chess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chess-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-xs-12">
        <?= $form->field($model, 'Status')
            ->widget(
                \kartik\widgets\SwitchInput::classname(),
                [
                    'value' => true,
                    'type' => \kartik\widgets\SwitchInput::RADIO,
                    'items' => [
                        ['label' => Yii::t('app', 'Доступна'), 'value' => 'Доступна'],
                        ['label' => Yii::t('app', 'Продана'), 'value' => 'Продана'],
                        ['label' => Yii::t('app', 'Бронь'), 'value' => 'Бронь'],
                    ],
                    'pluginOptions' =>
                        [
                            'size' => 'large',
                            'onColor' => 'success',
                            'offColor' => 'danger',
                            'onText' => Yii::t('app', 'Active'),
                            'offText' => Yii::t('app', 'Inactive'),
                        ],
                ]
            )
        ;
        ?>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'key')->textInput() ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'DataSdachi')->widget(
                    \kartik\date\DatePicker::class,
                    [
                        'name' => 'DateActual',
                        'type' => \kartik\date\DatePicker::TYPE_COMPONENT_PREPEND,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                        ],
                    ]
                ) ?>

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'm2obshaya')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Etash')->textInput() ?>

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IDApartment')->widget(
                    \kartik\select2\Select2::classname(), [
                    'data' =>
                        \yii\helpers\ArrayHelper::map(
                            \common\models\Apartment::find()
                                ->select('id,key')
                                ->asArray()
                                ->all(),
                            "key", "key"),
                    'options' => ['placeholder' => 'Parent'],
                    'pluginOptions' => ['allowClear' => true],
                ]) ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IDHouse')->widget(
                    \kartik\select2\Select2::classname(), [
                    'data' =>
                        \yii\helpers\ArrayHelper::map(
                            \common\models\House::find()
                                ->select('id,key')
                                ->asArray()
                                ->all(),
                            "key", "key"),
                    'options' => ['placeholder' => 'Parent'],
                    'pluginOptions' => ['allowClear' => true],
                ]) ?>

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IDShka')->widget(
                    \kartik\select2\Select2::classname(), [
                    'data' =>
                        \yii\helpers\ArrayHelper::map(
                            \common\models\Shka::find()
                                ->select('id,key,NameShka')
                                ->asArray()
                                ->all(),
                            "key", "NameShka", 'key'),
                    'options' => ['placeholder' => 'Parent'],
                    'pluginOptions' => ['allowClear' => true],
                ]) ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Top3kv')->textInput() ?>

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Number')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Podezd')->textInput() ?>

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Price2')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Price3')->textInput() ?>

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'PriceBase')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'SposobOtobrasheniya')->textInput() ?>

            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
