<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ApartmentControl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="apartment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'key') ?>

    <?= $form->field($model, 'BalkonLodgiya') ?>

    <?= $form->field($model, 'DopOption') ?>

    <?= $form->field($model, 'DopOtdelka') ?>

    <?php // echo $form->field($model, 'DveriVhod') ?>

    <?php // echo $form->field($model, 'Electro') ?>

    <?php // echo $form->field($model, 'ID House') ?>

    <?php // echo $form->field($model, 'ID Shka') ?>

    <?php // echo $form->field($model, 'IM1') ?>

    <?php // echo $form->field($model, 'IM2') ?>

    <?php // echo $form->field($model, 'IM3') ?>

    <?php // echo $form->field($model, 'IM4') ?>

    <?php // echo $form->field($model, 'IM5') ?>

    <?php // echo $form->field($model, 'IM6') ?>

    <?php // echo $form->field($model, 'IM7') ?>

    <?php // echo $form->field($model, 'IM8') ?>

    <?php // echo $form->field($model, 'IM9') ?>

    <?php // echo $form->field($model, 'IM10') ?>

    <?php // echo $form->field($model, 'KolKomnat') ?>

    <?php // echo $form->field($model, 'Okna') ?>

    <?php // echo $form->field($model, 'OpisanieKvartiri') ?>

    <?php // echo $form->field($model, 'OsteklenieLodgii') ?>

    <?php // echo $form->field($model, 'OsteklenieOkna') ?>

    <?php // echo $form->field($model, 'Otoplenie') ?>

    <?php // echo $form->field($model, 'Otopleniye') ?>

    <?php // echo $form->field($model, 'Peregorodki') ?>

    <?php // echo $form->field($model, 'Poshar') ?>

    <?php // echo $form->field($model, 'Prem1') ?>

    <?php // echo $form->field($model, 'Prem2') ?>

    <?php // echo $form->field($model, 'Prem3') ?>

    <?php // echo $form->field($model, 'Prem4') ?>

    <?php // echo $form->field($model, 'Prem5') ?>

    <?php // echo $form->field($model, 'Prem6') ?>

    <?php // echo $form->field($model, 'Santehnika') ?>

    <?php // echo $form->field($model, 'Schetchik') ?>

    <?php // echo $form->field($model, 'SposobOtobrasheniya') ?>

    <?php // echo $form->field($model, 'StoronaSveta') ?>

    <?php // echo $form->field($model, 'Vid') ?>

    <?php // echo $form->field($model, 'Vitrash') ?>

    <?php // echo $form->field($model, 'm2balkon') ?>

    <?php // echo $form->field($model, 'm2kuhna') ?>

    <?php // echo $form->field($model, 'm2lodshiya') ?>

    <?php // echo $form->field($model, 'm2obshaya') ?>

    <?php // echo $form->field($model, 'm2shilaya') ?>

    <?php // echo $form->field($model, 'visotaPotolka') ?>

    <?php // echo $form->field($model, 'enable') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
