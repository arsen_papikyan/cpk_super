<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ApartmentControl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Apartments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="apartment-index table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Apartment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'key:ntext',
            'BalkonLodgiya:ntext',
            'DopOption:ntext',
            'DopOtdelka:ntext',
            //'DveriVhod:ntext',
            //'Electro:ntext',
            //'ID House:ntext',
            //'ID Shka:ntext',

            [
                'attribute' => 'IDShka',
                'value' => function ($data) {
                    return \common\models\Shka::findOne(['key' => $data['ID Shka']])['NameShka'];
                },
            ],
            [
                'attribute' => 'IDHouse',
                'value' => function ($data) {
                    return$data['ID House'];
                },
            ],

            //'IM1:ntext',
            //'IM2:ntext',
            //'IM3:ntext',
            //'IM4:ntext',
            //'IM5:ntext',
            //'IM6:ntext',
            //'IM7:ntext',
            //'IM8:ntext',
            //'IM9:ntext',
            //'IM10:ntext',
            //'KolKomnat:ntext',
            //'Okna:ntext',
            //'OpisanieKvartiri:ntext',
            //'OsteklenieLodgii:ntext',
            //'OsteklenieOkna:ntext',
            //'Otoplenie:ntext',
            //'Otopleniye:ntext',
            //'Peregorodki:ntext',
            //'Poshar:ntext',
            //'Prem1:ntext',
            //'Prem2:ntext',
            //'Prem3:ntext',
            //'Prem4:ntext',
            //'Prem5:ntext',
            //'Prem6:ntext',
            //'Santehnika:ntext',
            //'Schetchik:ntext',
            //'SposobOtobrasheniya:ntext',
            //'StoronaSveta:ntext',
            //'Vid:ntext',
            //'Vitrash:ntext',
            //'m2balkon:ntext',
            //'m2kuhna:ntext',
            //'m2lodshiya:ntext',
            //'m2obshaya:ntext',
            //'m2shilaya:ntext',
            //'visotaPotolka:ntext',
            //'enable',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{duplicate}  {view} {update} {delete}',
                'buttons' =>
                    [
                        'duplicate' => function ($url, $model) {
                            return Html::a('<span  class="fa fa-files-o" aria-hidden="true""></span>', $url, [
                                'title' => Yii::t('app', 'Duplicate'),
                            ]);
                        },
                    ],
            ],

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
