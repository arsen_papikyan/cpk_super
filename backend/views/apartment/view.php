<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Apartment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Apartments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="apartment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'key:ntext',
            'BalkonLodgiya:ntext',
            'DopOption:ntext',
            'DopOtdelka:ntext',
            'DveriVhod:ntext',
            'Electro:ntext',
            'ID House:ntext',
            'ID Shka:ntext',
            'IM1:ntext',
            'IM2:ntext',
            'IM3:ntext',
            'IM4:ntext',
            'IM5:ntext',
            'IM6:ntext',
            'IM7:ntext',
            'IM8:ntext',
            'IM9:ntext',
            'IM10:ntext',
            'KolKomnat:ntext',
            'Okna:ntext',
            'OpisanieKvartiri:ntext',
            'OsteklenieLodgii:ntext',
            'OsteklenieOkna:ntext',
            'Otoplenie:ntext',
            'Otopleniye:ntext',
            'Peregorodki:ntext',
            'Poshar:ntext',
            'Prem1:ntext',
            'Prem2:ntext',
            'Prem3:ntext',
            'Prem4:ntext',
            'Prem5:ntext',
            'Prem6:ntext',
            'Santehnika:ntext',
            'Schetchik:ntext',
            'SposobOtobrasheniya:ntext',
            'StoronaSveta:ntext',
            'Vid:ntext',
            'Vitrash:ntext',
            'm2balkon:ntext',
            'm2kuhna:ntext',
            'm2lodshiya:ntext',
            'm2obshaya:ntext',
            'm2shilaya:ntext',
            'visotaPotolka:ntext',
            'enable',
        ],
    ]) ?>

</div>
