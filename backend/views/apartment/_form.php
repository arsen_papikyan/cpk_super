<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Apartment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="apartment-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'key')->textInput() ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'BalkonLodgiya')->textInput()?>
            </div>
        </div>
    </div> 
    
    
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'DopOption')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'DopOtdelka')->textInput() ?>

            </div>
        </div>
    </div> 
    
    
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'DveriVhod')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Electro')->textInput() ?>

            </div>
        </div>
    </div>
    
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">

            </div>
            <div class="col-xs-6">

            </div>
        </div>
    </div>


    
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, "IDHouse")->widget(
                    \kartik\select2\Select2::classname(), [
                    'data' =>
                        \yii\helpers\ArrayHelper::map(
                            \common\models\House::find()
                                ->select('id,key')
                                ->asArray()
                                ->all(),
                            "key", "key"),
                    'options' => ['placeholder' => 'Parent'],
                    'pluginOptions' => ['allowClear' => true],
                ])
                ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IDShka')->widget(
                    \kartik\select2\Select2::classname(), [
                    'data' =>
                        \yii\helpers\ArrayHelper::map(
                            \common\models\Shka::find()
                                ->select('id,key,NameShka')
                                ->asArray()
                                ->all(),
                            "key", "NameShka", 'key'),
                    'options' => ['placeholder' => 'Parent'],
                    'pluginOptions' => ['allowClear' => true],
                ])?>
            </div>
        </div>
    </div>
   
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IM1')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IM2')->textInput() ?>
            </div>
        </div>
    </div>



    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IM3')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IM4')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IM5')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IM6')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IM7')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IM8')->textInput() ?>
            </div>
        </div>
    </div>



    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IM9')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IM10')->textInput() ?>
            </div>
        </div>
    </div>



    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'KolKomnat')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Okna')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'OpisanieKvartiri')->textarea(['row'=>3]) ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'OsteklenieLodgii')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'OsteklenieOkna')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Otoplenie')->textInput() ?>
            </div>
        </div>
    </div>



    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Otopleniye')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Peregorodki')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Poshar')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Prem1')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Prem2')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Prem3')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Prem4')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Prem5')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Prem6')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Santehnika')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Schetchik')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'SposobOtobrasheniya')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'StoronaSveta')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Vid')->textInput() ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Vitrash')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'm2balkon')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'm2kuhna')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'm2lodshiya')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'm2obshaya')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'm2shilaya')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'visotaPotolka')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'enable')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
