<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Yii::$app->homeUrl ?>images/man.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->getIdentity()->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => Yii::t('app', 'User'), 'icon' => '', 'url' => ['/user/index']],
                    ['label' => Yii::t('app', 'Chess'), 'icon' => '', 'url' => ['/chess/index']],
                    ['label' => Yii::t('app', 'Apartment'), 'icon' => '', 'url' => ['/apartment/index']],
                    ['label' => Yii::t('app', 'House'), 'icon' => '', 'url' => ['/house/index']],
                    ['label' => Yii::t('app', 'Shka'), 'icon' => '', 'url' => ['/shka/index']],
                    ['label' => Yii::t('app', 'Zastroi'), 'icon' => '', 'url' => ['/zastroi/index']],
                    ['label' => Yii::t('app', 'Segment'), 'icon' => '', 'url' => ['/segment/index']],
                    ['label' => Yii::t('app', 'Personal'), 'icon' => '', 'url' => ['/personal/index']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
