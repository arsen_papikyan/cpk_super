<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Personal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="personal-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'key')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Email')->textInput() ?>

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Family')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'IDSegment')->widget(
                    \kartik\select2\Select2::classname(), [
                    'data' =>
                        \yii\helpers\ArrayHelper::map(
                            \common\models\Segment::find()
                                ->select('id,key')
                                ->asArray()
                                ->all(),
                            "key", "key"),
                    'options' => ['placeholder' => 'Parent'],
                    'pluginOptions' => ['allowClear' => true],
                ]) ?>

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Middle')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Name')->textInput() ?>

            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Phone')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Position')->textInput() ?>

            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    </div>


    <div class="col-xs-12">
<!---->
<!---->
<!--        --><?php
//
        $modelId = $model->id;
        $uploadImagesUrl = \yii\helpers\Url::to(['/personal/upload-images?id=' . $modelId]);

        $imagesOptions = [];
        $imgPath = [];

        if (!$model->isNewRecord) {
            $imgName = $model->img_name;
            $imgFullPath = dirname(dirname(dirname(__DIR__))) . "/cpk-super.club/web/images/upload/". $imgName;

            if (!empty($imgName)) {
                $deleteUrl = \yii\helpers\Url::to(["/personal/delete-file?id=" . $modelId]);

                $imgPath[] = \yii\helpers\Url::to('http://cpk-super.club/') . $imgName;
                $size = 0;
                if (file_exists($imgFullPath)) {
                    $size = filesize($imgFullPath);
                }
                $imagesOptions[] = [
//                'caption' => $model->title,
                    'url' => $deleteUrl,
                    'size' => $size,
                    'key' => $modelId,
                ];
            }
        }
      ?>
<?=$form->field($model, 'img_name')
            ->widget(
                \kartik\widgets\FileInput::class,
                [

                    'options' =>
                        [
                            'accept' => 'image/*',
                            'multiple' => false,
                        ],
                    'pluginOptions' =>
                        [
                            'previewFileType' => 'image',
                            "uploadAsync" => true,
                            'showPreview' => true,
                            'showUpload' => $model->isNewRecord ? false : true,
                            'showCaption' => false,
                            'showDrag' => false,
                            'uploadUrl' => $uploadImagesUrl,
                            'initialPreviewConfig' => $imagesOptions,
                            'initialPreview' => $imgPath,
                            'initialPreviewAsData' => true,
                            'initialPreviewShowDelete' => true,
                            'overwriteInitial' => true,
                            'resizeImages' => true,
                            'layoutTemplates' => [!$model->isNewRecord ?: 'actionUpload' => '',],
                        ],
                ]);
      ?>
    </div>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
