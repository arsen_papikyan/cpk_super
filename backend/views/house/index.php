<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HouseControl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Houses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="house-index table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create House'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'key:ntext',
//            'DopZona:ntext',
//            'Etash:ntext',
//            'Gaz:ntext',
//            'IDShka:ntext',
//            'KolKvartir:ntext',
//            'KolLift:ntext',
//            'KolPodezd:ntext',
//            'Konsersh:ntext',
//            'Material:ntext',
//            'OtdelkaZon:ntext',
//            'Otoplenie:ntext',
//            'PodzemnayaParking:ntext',
//            'Shouroom:ntext',
//            'Territory:ntext',
            [
                'attribute' => 'IDShka',
                'value' => function ($data) {
                    return \common\models\Shka::findOne(['key' => $data['ID Shka']])->NameShka;
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{duplicate}  {view} {update} {delete}',
                'buttons' =>
                    [
                        'duplicate' => function ($url, $model) {
                            return Html::a('<span  class="fa fa-files-o" aria-hidden="true""></span>', $url, [
                                'title' => Yii::t('app', 'Duplicate'),
                            ]);
                        },
                    ],
            ],
        ],
    ]); ?>


</div>
