<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\House */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="house-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'key')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'DopZona')->textInput() ?>

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Etash')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Gaz')->textInput() ?>

            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'IDShka')->widget(
                    \kartik\select2\Select2::classname(), [
                    'data' =>
                        \yii\helpers\ArrayHelper::map(
                            \common\models\Shka::find()
                                ->select('id,key,NameShka')
                                ->asArray()
                                ->all(),
                            "key", "NameShka",'key'),
                    'options' => ['placeholder' => 'Parent'],
                    'pluginOptions' => ['allowClear' => true],
                ]) ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'KolKvartir')->textInput() ?>

            </div>
        </div>
    </div>





    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'KolLift')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'KolPodezd')->textInput() ?>

            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'Konsersh')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Material')->textInput() ?>

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'OtdelkaZon')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Otoplenie')->textInput() ?>

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'PodzemnayaParking')->textInput() ?>

            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'Shouroom')->textInput() ?>

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <?= $form->field($model, 'Territory')->textInput() ?>

    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
