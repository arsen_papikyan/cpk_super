<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\HouseControl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="house-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'key') ?>

    <?= $form->field($model, 'DopZona') ?>

    <?= $form->field($model, 'Etash') ?>

    <?= $form->field($model, 'Gaz') ?>

    <?php // echo $form->field($model, 'ID Shka') ?>

    <?php // echo $form->field($model, 'KolKvartir') ?>

    <?php // echo $form->field($model, 'KolLift') ?>

    <?php // echo $form->field($model, 'KolPodezd') ?>

    <?php // echo $form->field($model, 'Konsersh') ?>

    <?php // echo $form->field($model, 'Material') ?>

    <?php // echo $form->field($model, 'OtdelkaZon') ?>

    <?php // echo $form->field($model, 'Otoplenie') ?>

    <?php // echo $form->field($model, 'PodzemnayaParking') ?>

    <?php // echo $form->field($model, 'Shouroom') ?>

    <?php // echo $form->field($model, 'Territory') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
