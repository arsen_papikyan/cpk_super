<?php

namespace backend\controllers;

use Yii;
use common\models\Shka;
use backend\models\ShkaControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ShkaController implements the CRUD actions for Shka model.
 */
class ShkaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Shka models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShkaControl();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Shka model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Shka model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Shka();

        if ($model->load(Yii::$app->request->post())) {

            $model['DrugieObekt1-3'] = $model['DrugieObekt1_3'];
            $model['Giper1-3'] = $model['Giper1_3'];
            $model['ID Segment'] = $model['IDSegment'];
            $model['Magazin1-3'] = $model['Magazin1_3'];
            $model['Ostanovka1-3'] = $model['Ostanovka1_3'];
            $model['Park1-3'] = $model['Park1_3'];
            $model['Rinok1-3'] = $model['Rinok1_3'];
            $model['Sadik1-3'] = $model['Sadik1_3'];
            $model['Shkola1-3'] = $model['Shkola1_3'];
            $model['Sport1-3'] = $model['Sport1_3'];
            $model['ID Apartment'] = $model['IDApartment'];
            $model->DateA = date('yy-m-d', time());

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Shka model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model['DrugieObekt1_3'] = $model['DrugieObekt1-3'];
        $model['Giper1_3'] = $model['Giper1-3'];
        $model['IDSegment'] = $model['ID Segment'];
        $model['Magazin1_3'] = $model['Magazin1-3'];
        $model['Ostanovka1_3'] = $model['Ostanovka1-3'];
        $model['Park1_3'] = $model['Park1-3'];
        $model['Rinok1_3'] = $model['Rinok1-3'];
        $model['Sadik1_3'] = $model['Sadik1-3'];
        $model['Shkola1_3'] = $model['Shkola1-3'];
        $model['Sport1_3'] = $model['Sport1-3'];
        $model['IDApartment'] = $model['ID Apartment'];

        if ($model->load(Yii::$app->request->post()) ) {
            $model['DrugieObekt1-3'] = $model['DrugieObekt1_3'];
            $model['Giper1-3'] = $model['Giper1_3'];
            $model['ID Segment'] = $model['IDSegment'];
            $model['Magazin1-3'] = $model['Magazin1_3'];
            $model['Ostanovka1-3'] = $model['Ostanovka1_3'];
            $model['Park1-3'] = $model['Park1_3'];
            $model['Rinok1-3'] = $model['Rinok1_3'];
            $model['Sadik1-3'] = $model['Sadik1_3'];
            $model['Shkola1-3'] = $model['Shkola1_3'];
            $model['Sport1-3'] = $model['Sport1_3'];
            $model['ID Apartment'] = $model['IDApartment'];
            $model->DateA = date('yy-m-d', time());

            if ($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Shka model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Shka model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shka the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shka::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionDuplicate($id)
    {
        $model = $this->findModel($id);
        $modelDuplicate = new  Shka();
        $modelDuplicate->attributes = $model->attributes;
        $modelDuplicate->DateA = date('yy-m-d', time());
        $modelDuplicate->save();

        return $this->redirect(['index']);
    }
}
