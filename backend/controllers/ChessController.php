<?php

namespace backend\controllers;

use Yii;
use common\models\Chess;
use backend\models\ChessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ChessController implements the CRUD actions for Chess model.
 */
class ChessController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Chess models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ChessControl();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Chess model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Chess model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Chess();

        if ($model->load(Yii::$app->request->post())) {
            $model['ID Apartment'] = $model['IDApartment'];
            $model['ID House'] = $model['IDHouse'];
            $model['ID Shka'] = $model['IDShka'];
            $model->DateActual = date('yy-m-d', time());
            $model->Price2 = $model->PriceBase + (50 * $model->Price2);
            $model->Price3 = $model->PriceBase + (50 * $model->Price3);
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Chess model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model['IDApartment'] = $model['ID Apartment'];
        $model['IDHouse'] = $model['ID House'];
        $model['IDShka'] = $model['ID Shka'];

        if ($model->load(Yii::$app->request->post())) {
            $model['ID Apartment'] = $model['IDApartment'];
            $model['ID House'] = $model['IDHouse'];
            $model['ID Shka'] = $model['IDShka'];
            $model->DateActual = date('yy-m-d', time());
            $model->Price2 = $model->PriceBase + (50 * $model->Price2);
            $model->Price3 = $model->PriceBase + (50 * $model->Price3);

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Chess model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Chess model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Chess the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Chess::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionDuplicate($id)
    {
        $model = $this->findModel($id);
        $modelDuplicate = new  Chess();
        $modelDuplicate->attributes = $model->attributes;
        $modelDuplicate->DateActual = date('yy-m-d', time());
        $modelDuplicate->save(false);

        return $this->redirect(['index']);
    }
}
